<?php

namespace App\Repository;

use App\Entity\OtpGenerator;
use Carbon\Carbon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method OtpGenerator|null find($id, $lockMode = null, $lockVersion = null)
 * @method OtpGenerator|null findOneBy(array $criteria, array $orderBy = null)
 * @method OtpGenerator[]    findAll()
 * @method OtpGenerator[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OtpGeneratorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OtpGenerator::class);
    }

    /**
     * @param $userId
     * @param $otpCode
     * @return OtpGenerator[]
     */
    public function findUserWithValidOtp($userId, $otpCode)
    {
        return $this->createQueryBuilder('otp')
            ->andWhere('otp.user_id = :user_id')
            ->setParameter('user_id', $userId)
            ->andWhere('otp.otp_code = :otp_code')
            ->setParameter('otp_code', $otpCode)
            ->andWhere('otp.timer > :timer')
            ->setParameter('timer', Carbon::now('GMT+2'))
            ->getQuery()
            ->getResult();
    }

    public function deleteExpiredOtp()
    {
        return $this->createQueryBuilder('otp')
            ->andWhere('otp.timer < :timer')
            ->setParameter('timer', Carbon::now('GMT+2'))
            ->getQuery()
            ->getResult();
    }
}
