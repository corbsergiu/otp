<?php


namespace App\Service;

use App\Entity\OtpGenerator;
use Carbon\Carbon;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegisterOtpService
{
    protected $entityManager;
    protected $validator;
    protected $conn;

    public function __construct(EntityManagerInterface $entityManager, ValidatorInterface $validator, Connection $conn)
    {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->conn = $entityManager;
    }

    public function registerOtp($userId)
    {
        $otpCodeGenerator = random_int(11111, 99999);
        $expirationTime = Carbon::now('GMT+2')->addMinutes(2);
        $otpRegisterEntity = new OtpGenerator();
        $otpRegisterEntity
            ->setUserId($userId)
            ->setOtpCode($otpCodeGenerator)
            ->setTimer($expirationTime);
//        $errors = $this->validator->validate($otpRegisterEntity);
//        if (count($errors)) {
//            $messages = [];
//            foreach ($errors as $error) {
//                $messages[] = $error->getMessage();
//            }
//            return new JsonResponse(['errors' => $messages], 400);
//        }
        $this->conn->beginTransaction();
        try {
            $this->entityManager->persist($otpRegisterEntity);
            $this->entityManager->flush();
            $this->conn->commit();
        } catch (\Exception $exception) {
            $this->conn->rollBack();
            return new JsonResponse(['error' => $exception]);
        }
        return $response = [
            'user_id' => $userId,
            'otp_code' => $otpCodeGenerator
        ];
    }

    public function validateOtp($userId, $otp)
    {
        $repository = $this->entityManager->getRepository(OtpGenerator::class);
        $findUser = $repository->findUserWithValidOtp($userId, $otp);
        $expiredOtp = $repository->deleteExpiredOtp();
        foreach ($expiredOtp as $value) {
            $this->entityManager->remove($value);
        }
        $this->entityManager->flush();
        if (empty($findUser)) {
            $response = false;
        } else {
            $response = true;
        }
        return $response;
    }
}
