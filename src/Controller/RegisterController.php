<?php

namespace App\Controller;

use App\Entity\OtpGenerator;
use App\Service\RegisterOtpService;
use Carbon\Carbon;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegisterController extends AbstractController
{
    protected $otpService;
    protected $validator;

    public function __construct(RegisterOtpService $otpService, ValidatorInterface $validator)
    {
        $this->otpService = $otpService;
        $this->validator = $validator;
    }

    /**
     * @Route("/register", methods="POST");
     * @param Request $request
     * @return JsonResponse
     */
    public function storeOtp(Request $request)
    {
        $userId = $request->query->get('user_id');
        $response = $this->otpService->registerOtp($userId);
        if (intval($userId) === 0) {
            return new JsonResponse(['error' => 'user id field is required'], JsonResponse::HTTP_NOT_FOUND);
        } else {
            return new JsonResponse($response, JsonResponse::HTTP_OK);

        }
    }

    /**
     * @Route("/validate-otp", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function checkGeneratedOtp(Request $request)
    {
        $userId = $request->query->get('user_id');
        $otp = $request->query->get('otp_code');
        $otpRegisterEntity = new OtpGenerator();
        $otpRegisterEntity
            ->setOtpCode($otp)
            ->setUserId($userId);
        $errors = $this->validator->validate($otpRegisterEntity);
        $errorMessages = [];
        if (count($errors)) {
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }
            return new JsonResponse(['errors' => $errorMessages], 400);
        }
        $validOtp = $this->otpService->validateOtp($userId, $otp);

        if ($validOtp == false) {
            return new JsonResponse($validOtp, JsonResponse::HTTP_UNAUTHORIZED);
        } else {
            return new JsonResponse($validOtp, JsonResponse::HTTP_OK);
        }
    }
}
