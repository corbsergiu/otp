<?php

namespace App\Entity;

use Doctrine\DBAL\Types\TimeImmutableType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\OtpGeneratorRepository")
 */
class OtpGenerator
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="user_id")
     * @ORM\ManyToOne(targetEntity="App\Entity\Users",inversedBy="otp")
     * @Assert\Type(type="numeric" , message="user_id must have an integer value")
     * @Assert\NotBlank(message="user_id field is required")
     *
     */
    private $user_id;

    /**
     * @ORM\Column(name="otp_code")
     * @Assert\Type(type="numeric", message="otp_code must have an integer value")
     * @Assert\NotBlank(message="otp_code field is required")
     */
    private $otp_code;

    /**
     * @ORM\Column(type="datetime")
     */
    private $timer;

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    public function setUserId($user_id): self
    {
        $this->user_id = $user_id;
        return $this;
    }

    public function setOtpCode($otp_code): self
    {
        $this->otp_code = $otp_code;
        return $this;
    }

    public function setTimer(\DateTimeInterface $timer): self
    {
        $this->timer = $timer;
        return $this;
    }
}
