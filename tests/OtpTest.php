<?php


namespace App\Tests;

use App\Entity\OtpGenerator;
use App\Service\RegisterOtpService;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OtpTest extends TestCase
{
    protected $entityManager;
    protected $validator;
    protected $conn;

    public function __construct(EntityManagerInterface $entityManager, ValidatorInterface $validator, Connection $conn)
    {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->conn = $conn;
    }

    public function testOtpGenerated()
    {
        $otpService = new RegisterOtpService($this->entityManager, $this->validator, $this->conn);
        $result = $otpService->registerOtp(4);
        $this->assertEquals(200, $result->getStatusCode());
    }
}
